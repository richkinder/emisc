;;; emisc-post.el --- Miscellaneous custom functions  -*- lexical-binding: t -*-

;; Copyright (C) 2019 Richard Kinder

;;; Commentary:

;; This is a collection of miscellaneous functions to be loaded after other
;; packages.

;;; Code:

(require 'ample-regexps)
(require 'bookmark)
(require 'cus-edit)
(require 'dash)
(require 'emisc-pre)
(require 'f)
(require 'git-gutter)
(require 'hydra)
(require 'multishell)
(require 'multishell-list)
(require 's)
(require 'savehist)

(defgroup emisc nil
  "Customization group for emisc customizations."
  :group 'local)

;;; hack blocks

(defun emisc-insert-hack-block (&optional arg)
  "Insert comment block indicating code that should be removed prior to use.

This is language-general. If applied to a region, it wraps the
region. With a prefix arg, it also inserts a blank line before
and after the block."
  (interactive "P")
  (let ((open-string "HACK")
        (close-string "/HACK")
        (pt-indentation nil))
    (if (region-active-p)
        (progn
          ;; Ensure point is before mark.
          (when (> (point) (mark))
            (exchange-point-and-mark))
          ;; Open the hack block.
          (when (> (point)
                   (+ (line-beginning-position) (current-indentation)))
            ;; Do not include the line point is on.
            (forward-line))
          (setq pt-indentation (current-indentation))
          (beginning-of-line)
          (newline)
          (when arg (newline))
          (forward-line -1)
          (comment-indent)
          ;; Coerce to the desired indentation.
          (delete-region (line-beginning-position)
                         (+ (line-beginning-position) (current-indentation)))
          (beginning-of-line)
          (insert (make-string pt-indentation ? ))
          (end-of-line)
          (copy-region-as-kill (line-beginning-position) (line-end-position))
          (insert open-string)

          ;; Close the hack block.
          (exchange-point-and-mark)
          (when (<= (point)
                    (+ (line-beginning-position) (current-indentation)))
            ;; Do not include the line point is on.
            (forward-line -1))
          (end-of-line)
          (newline)
          (yank)
          (end-of-line)
          (insert close-string)
          (when arg (newline)))
      ;; Open the hack block.
      (setq pt-indentation (current-indentation))
      (move-beginning-of-line nil)
      (newline)
      (forward-line -1)
      (when arg (newline))
      (comment-indent)
      ;; Coerce to the desired indentation.
      (delete-region (line-beginning-position)
                     (+ (line-beginning-position) (current-indentation)))
      (beginning-of-line)
      (insert (make-string pt-indentation ? ))
      (end-of-line)
      (copy-region-as-kill (line-beginning-position) (line-end-position))
      (insert open-string)

      ;; Close the hack block.
      (newline)
      (indent-for-tab-command)
      (save-excursion
        (newline)
        (yank)
        (end-of-line)
        (insert close-string)
        (when arg (newline))))))

(defun emisc-remove-hack-block (&optional arg)
  "Remove hack block inserted by `emisc-insert-hack-block'."
  (interactive "P")
  (let ((hack-forward-open-pt 
         (save-excursion
           (search-forward-regexp "[^/]HACK" nil t)))
        (hack-forward-close-pt 
         (save-excursion
           (search-forward-regexp "/HACK" nil t)))
        (hack-back-open-pt 
         (save-excursion
           (search-backward-regexp "[^/]HACK" nil t)))
        (hack-back-close-pt 
         (save-excursion
           (search-backward-regexp "/HACK" nil t))))
    (if (and
         (and hack-back-open-pt hack-forward-close-pt)
         (if hack-back-close-pt
             (> hack-back-open-pt hack-back-close-pt)
           t)
         (if hack-forward-open-pt
             (> hack-forward-open-pt hack-forward-close-pt)
           t))
        (kill-region
         (progn
           (goto-char hack-back-open-pt)
           (line-beginning-position))
         (progn
           (goto-char hack-forward-close-pt)
           (forward-line)
           (line-beginning-position)))
      (message "Not in a valid hack block"))))

;;; jump-get

(defvar emisc--jump-get-recall-target nil
  "Variable for storing the recall location when a jump-get is initiated")

(defun emisc-jump-get-char-2-all-frames (prefix)
  "Perform a jump-get excursion on all frames using `avy-goto-char-2'.
With a prefix arg, suppress jump-get-recall."
  (interactive "P")
  ;; Clear any echo-keystrokes
  (message nil)
  (let ((avy-all-windows 'all-frames))
    (emisc--jump-get 'avy-goto-char-2 prefix)))

(defun emisc-jump-get-word-1-all-frames (prefix)
  "Perform a jump-get excursion in this window using `avy-goto-word-1'.
With a prefix arg, suppress jump-get-recall."
  (interactive "P")
  ;; Clear any echo-keystrokes
  (message nil)
  (let ((avy-all-windows 'all-frames))
    (emisc--jump-get 'avy-goto-word-1 prefix)))

(defun emisc--jump-get (jumping-fcn suppressp)
  "Apply a recall if there is a non-zero-width region active."
  (when (not (region-active-p))
    (emisc--record-current))
  (cond
   ((and (region-active-p)
         (not (= (point) (mark)))
         emisc--jump-get-params
         (not suppressp))
    (emisc--jump-get-recall))
   (t
    (emisc--jump-get-normal jumping-fcn))))

(defun emisc--record-current ()
  "Store current location as the recall target."
  (setq emisc--jump-get-params `(,(current-window-configuration)
                                 ,(selected-window)
                                 ,(point))))

(defun emisc--jump-get-recall ()
  "Yank the region to the location specified in `emisc--jump-get-params'."
  (kill-ring-save nil nil t)
  (set-window-configuration (nth 0 emisc--jump-get-params))
  (when (eq (selected-window) (nth 1 emisc--jump-get-params))
    ;; `current-window-configuration' deliberately does not record point
    ;; in the current window. So point must be restored explicitly.
    (goto-char (nth 2 emisc--jump-get-params)))
  (yank)
  (emisc-try-autowiden))

(defun emisc--jump-get-normal (jumping-fcn)
  ;; Remove prefix arg lest it affect the jumping-fcn.
  (let ((current-prefix-arg nil))
    (call-interactively jumping-fcn))
  (emisc-try-autowiden))

;;; ace-mc

(defun emisc-ace-mc-add-single-cursor-word ()
  "Use ace-jump-word-mode to add a single cursor.

This function was created (as opposed to just using a lambda) so that
.mc-lists.el can track it under the `mc/cmds-to-run-once' variable, which is
critical."
  (interactive)
  (ace-mc-add-multiple-cursors 0 t))

(defun emisc-ace-mc-add-single-cursor-char ()
  "Use ace-jump-char-mode to add a single cursor

This function was created (as opposed to just using a lambda) so that
.mc-lists.el can track it under the `mc/cmds-to-run-once' variable, which is
critical."
  (interactive)
  (ace-mc-add-multiple-cursors 4 t))

(defun emisc-ace-mc-add-multiple-cursor-word ()
  "Use ace-jump-word-mode to add multiple cursors

This function was created (as opposed to just using a lambda) so that
.mc-lists.el can track it under the `mc/cmds-to-run-once' variable, which is
critical."
  (interactive)
  (ace-mc-add-multiple-cursors 0 nil))

(defun emisc-ace-mc-add-multiple-cursor-char ()
  "Use ace-jump-char-mode to add multiple cursors

This function was created (as opposed to just using a lambda) so that
.mc-lists.el can track it under the `mc/cmds-to-run-once' variable, which is
critical."
  (interactive)
  (ace-mc-add-multiple-cursors 4 nil))

;;; autowiden

(defcustom emisc-autowiden-frame-inds nil
  "Which frame indices to use autowiden on.

Frame index is from left-to-right."
  :type '(repeat integer)
  :group 'emisc)

(defcustom emisc-autowiden-width 79
  "Number of columns used during autowiden."
  :type 'integer
  :group 'emisc)

(defun emisc-autowiden-get-wcs ()
  "Gather the window columns of the root window into an ordered list.

These are the immediate children of the root window.  I assume
this list is the positional ordering of the windows from left to
right, since `adjust-window-trailing-edge' only works from left
to right."
  (let* ((child (window-child (frame-root-window)))
         (wcs (list child)))
    (while (setq child (window-next-sibling child))        
      (push child wcs))
    (reverse wcs)))

(defun emisc-autowiden-selected-wc ()
  "Return the selected window column.

Climb the window-tree from the selected window until one of the
window columns is found."
  (let ((selected-wc (selected-window))
        (wcs (emisc-autowiden-get-wcs)))
    (while (not (member selected-wc wcs))
      (setq selected-wc (window-parent selected-wc)))
    selected-wc))

(defun emisc-autowiden-balance-wc ()
  "Balance the windows in the current window column.

A window column is a vertical combination of windows (see
`window-tree' in info) that is a child of the root window, which
is assumed to be a horizontal combination of windows."
  (interactive)
  ;; Verify that the root window is not live, and is a horizontal combination
  ;; of windows.
  (let* ((root (car (window-tree))))
    (unless (or (window-live-p root) (car root))
      (balance-windows (emisc-autowiden-selected-wc)))))

(defun emisc-autowiden-valid-target-p ()
  "Return t if the current window configuration is autowidenable.

It must meet all of these conditions:
1) The root window must be an internal window, not a live one. It
   could only be live if it were the only window (other than the
   minibuffer) because it is the root.
2) The root window must be a horizontal collection of windows, i.e. the
   first split, hierarchically, must be a horizontal one.
3) The selected window must not be the minibuffer."
  (let* ((root (car (window-tree))))
    (if (or
         (window-live-p root)
         (car root)
         (window-minibuffer-p))
        nil
      t)))

(defun emisc-try-autowiden ()
  (when (and (emisc-autowiden-p)
             (emisc-autowiden-valid-target-p))
    (emisc-autowiden)))

(defun emisc-autowiden ()
  "Autowiden the selected window column to `emisc-autowiden-width' columns.

A window column is a vertical combination of windows (see
`window-tree' in info) that is a child of the root window, which
is assumed to be a horizontal combination of windows."
  (interactive)
  (let* (columns-div
         columns-rem
         (wcs (emisc-autowiden-get-wcs))
         (wc-body-widths (loop for wc in wcs collect 
                               (emisc--autowiden-wc-body-width wc)))
         (free-column-count (- (apply '+ wc-body-widths) emisc-autowiden-width)))

    ;; Allocate widths to the window columns. There's no guarantee that
    ;; `free-column-count' is divisible evenly across columns, so allocate
    ;; any remainder to the first few columns.
    (setq columns-div (/ free-column-count (- (length wcs) 1)))
    (setq columns-rem (% free-column-count (- (length wcs) 1)))
    (loop for wc in wcs do
          ;; Determine the target width for this window column.
          ;; (progn)
          (if (eq wc (emisc-autowiden-selected-wc))
              (setq tgt-width emisc-autowiden-width)
            (setq tgt-width columns-div)
            (when (> columns-rem 0)
              (incf tgt-width)
              (decf columns-rem)))
          ;; Adjust the right edge of the window column unless it is the
          ;; rightmost.
          (when (window-next-sibling wc)
            (adjust-window-trailing-edge wc
                                         (- tgt-width
                                            (emisc--autowiden-wc-body-width wc))
                                         t)))
    ;; Ensure the window is scrolled all the way to the left.
    (set-window-hscroll (selected-window) 0)))

(defun emisc-autowiden-toggle ()
  "Toggle autowiden on the current frame."
  (interactive)
  (let ((frame-ind (emisc-selected-frame-ind)))
    (if (emisc-autowiden-p)
        (progn
          (setq emisc-autowiden-frame-inds
                (remove frame-ind emisc-autowiden-frame-inds))
          (message "Autowiden disabled on frame %d" frame-ind))
      (push frame-ind emisc-autowiden-frame-inds)
      (message "Autowiden enabled on frame %d" frame-ind))))

(defun emisc-autowiden-p ()
  (memq (emisc-selected-frame-ind) emisc-autowiden-frame-inds))

(defun emisc-ace-window-with-autowiden ()
  (interactive)
  (call-interactively 'ace-window)
  (emisc-try-autowiden))

(defun emisc--autowiden-wc-body-width (wc)
  "Return the body width of the current window column."
  (if (window-live-p wc)
      (window-body-width wc)
    (window-body-width (window-child wc))))

(defun emisc-delete-window ()
  "Delete window then select a logical next window.

First it tries the one below, failing that it at least tries to
stay in its window column by moving up, finally it goes to the
right, and otherwise it uses the default behavior of
`delete-window'."
  (interactive)
  (let ((win (selected-window)))
    (condition-case nil (windmove-down)
      (user-error
       (condition-case nil (windmove-up)
         (user-error
          (condition-case nil (windmove-right)
            (user-error nil))))))
    (delete-window win)))

;;; windmove

(defun emisc-windspill-right (&optional arg)
  "Windmove to the right, spilling over into the next frame if
necessary."
  (interactive)
  (condition-case nil (windmove-right)
    (user-error
     ;; We hit the wall of the frame, try to spill into an adjacent frame.
     (let* ((frames (emisc-frames-l-to-r))
            (spill-frame-ind (1+ (position (selected-frame)
                                           frames))))
       (if (< spill-frame-ind (length frames))
           (progn
             (select-frame (nth spill-frame-ind frames))
             (while (condition-case nil (windmove-left)
                      (user-error nil))))
         ;; There is no further frame to spill into, so call windmove again and
         ;; let it error.
         (windmove-right))))))

(defun emisc-windspill-left (&optional arg)
  "Windmove to the left, spilling over into the next frame if
necessary."
  (interactive)
  (condition-case nil (windmove-left)
    (user-error
     ;; We hit the wall of the frame, try to spill into an adjacent frame.
     (let* ((frames (emisc-frames-l-to-r))
            (spill-frame-ind (1- (position (selected-frame)
                                           frames))))
       (if (>= spill-frame-ind 0)
           (progn
             (select-frame (nth spill-frame-ind frames))
             (while (condition-case nil (windmove-right)
                      (user-error nil))))
         ;; There is no further frame to spill into, so call windmove again and
         ;; let it error.
         (windmove-left))))))

(defvar emisc-windmove-start-buffer nil
  "Buffer that was selected at the beginning of the windmove chain.")

(defvar emisc-windmove-start-window nil
  "Window that was selected at the beginning of the windmove chain.")

(defun emisc-windmove-shift-generic (the-windmove-func)
  "Generic function for the direction versions.

In previous versions, moving buffer 1 to the position of buffer 6
works well, but results in the following.

  +---+---+---+          +---+---+---+
  | 1 | 2 | 3 |          | 2 | 3 | 6 |
  +---+---+---+   --->   +---+---+---+
  | 4 | 5 | 6 |          | 4 | 5 | 1 |
  +---+---+---+          +---+---+---+

Moving one buffer to a desired location has changed the locations
of buffers 2, 3 and 6. Instead we should be swapping buffers. So
the same commands performed above now result in the following
more desirable outcome.

  +---+---+---+          +---+---+---+
  | 1 | 2 | 3 |          | 6 | 2 | 3 |
  +---+---+---+   --->   +---+---+---+
  | 4 | 5 | 6 |          | 4 | 5 | 1 |
  +---+---+---+          +---+---+---+

"
  (let ((orig-buffer (current-buffer))
        (orig-window (selected-window))
        (respect-start-p (member last-command '(emisc-windmove-shift-up
                                                emisc-windmove-shift-down
                                                emisc-windmove-shift-left
                                                emisc-windmove-shift-right))))
    (funcall the-windmove-func nil)
    (let ((new-buffer (current-buffer)) (new-window (selected-window)))
      (unless (or (minibufferp orig-buffer) (minibufferp new-buffer))
        (if respect-start-p
            (progn
              (select-window emisc-windmove-start-window)
              (switch-to-buffer new-buffer)
              (select-window orig-window)
              (switch-to-buffer emisc-windmove-start-buffer)
              (select-window new-window)
              (switch-to-buffer orig-buffer)
              (setq emisc-windmove-start-buffer new-buffer))
          (select-window orig-window)
          (switch-to-buffer new-buffer)
          (select-window new-window)
          (switch-to-buffer orig-buffer)
          (setq emisc-windmove-start-window orig-window)
          (setq emisc-windmove-start-buffer new-buffer))
        (emisc-try-autowiden)))))

(defun emisc-windmove-shift-up ()
  (interactive)
  (emisc-windmove-shift-generic 'windmove-up))

(defun emisc-windmove-shift-down ()
  (interactive)
  (emisc-windmove-shift-generic 'windmove-down))

(defun emisc-windmove-shift-left ()
  (interactive)
  (emisc-windmove-shift-generic 'emisc-windspill-left))

(defun emisc-windmove-shift-right ()
  (interactive)
  (emisc-windmove-shift-generic 'emisc-windspill-right))

(defun emisc-windmove-up ()
  (interactive)
  (windmove-up)
  (emisc-try-autowiden))

(defun emisc-windmove-down ()
  (interactive)
  (windmove-down)
  (emisc-try-autowiden))

(defun emisc-windmove-left ()
  (interactive)
  (emisc-windspill-left)
  (emisc-try-autowiden))

(defun emisc-windmove-right ()
  (interactive)
  (emisc-windspill-right)
  (emisc-try-autowiden))

(defvar emisc--mlo-window nil)

(defun emisc--clean-mlo ()
  "Clean the mode-line override if any."
  (interactive)
  (when emisc--mlo-window
    (with-selected-window emisc--mlo-window
      (set-window-parameter nil 'mode-line-format nil))
    (setq emisc--mlo-window nil)))

(defun emisc--mlo (fcn)
  "Call FCN, then set the mode line to look like it's active.

This is used during hydra-emisc-window to highlight the selected
window because I think during the hydra only the minibuffer is
considered active."
  (emisc--clean-mlo)
  (condition-case nil (funcall (symbol-function fcn))
    (user-error nil))
  (set-window-parameter (selected-window)
                        'mode-line-format (format-mode-line
                        mode-line-format))
  (setq emisc--mlo-window (selected-window)))

(defhydra hydra-emisc-window (:color red :hint nil)
  "
Move Pt    | _n_: left    _e_: down   _i_: up   _o_: right
Move Win   | _N_: left    _E_: down   _I_: up   _O_: right
Size       | _a_: -dx     _r_: -dy    _s_: +dy  _t_: +dx
Split      | _A_: h-left  _R_: v-down _S_: v-up _T_: h-right
Autowiden  | _b_: balance _f_: toggle _w_: now
Misc       | _u_: jump    _x_: delete _q_: quit"
  ("n" (progn (emisc--mlo 'emisc-windmove-left)))
  ("e" (progn (emisc--mlo 'emisc-windmove-down)))
  ("i" (progn (emisc--mlo 'emisc-windmove-up)))
  ("o" (progn (emisc--mlo 'emisc-windmove-right)))
  ("N" (progn (emisc--mlo 'emisc-windmove-shift-left)))
  ("E" (progn (emisc--mlo 'emisc-windmove-shift-down)))
  ("I" (progn (emisc--mlo 'emisc-windmove-shift-up)))
  ("O" (progn (emisc--mlo 'emisc-windmove-shift-right)))
  ("a" shrink-window-horizontally)
  ("r" shrink-window)
  ("s" enlarge-window)
  ("t" enlarge-window-horizontally)
  ("A" (progn (emisc--mlo 'split-window-right)))
  ("R" (progn (emisc--mlo 'emisc-split-window-above)))
  ("S" (progn (emisc--mlo 'split-window-below)))
  ("T" (progn (emisc--mlo 'emisc-split-window-left)))
  ("b" emisc-autowiden-balance-wc)
  ("f" emisc-autowiden-toggle)
  ("w" emisc-autowiden)
  ("u" (progn (emisc--mlo 'ace-window)))
  ("x" (progn (emisc--mlo 'emisc-delete-window)))
  ("q" emisc--clean-mlo :exit t))

;;; touchpad

(defcustom emisc-touchpad-str "Touchpad"
  "String used to search for touchpad device on xinput."
  :type 'string
  :group 'emisc)

(defvar emisc--touchpad-id-strs nil
  "List of xinput ids found to match `emisc-touchpad-str'.")

(defun emisc-toggle-touchpad ()
  (interactive)
  (let ((emisc--touchpad-id-strs (emisc--touchpad-id-strs)))
    (if emisc--touchpad-id-strs
        (emisc--set-touchpad-state (not (emisc--touchpad-enabled-p)))
      (message "No touchpad found."))))

(defun emisc-enable-touchpad ()
  (emisc--set-touchpad-state t))

(defun emisc-disable-touchpad ()
  (emisc--set-touchpad-state nil))

(defun emisc--set-touchpad-state (statep)
  (let ((state-num-str (if statep "1" "0"))
        (state-desc-str (if statep "enabled" "disabled")))
    (dotimes (id-str-ind (length emisc--touchpad-id-strs))
      (with-temp-buffer
        (shell-command (concat "xinput set-prop "
                               (nth id-str-ind emisc--touchpad-id-strs)
                               " \"Device Enabled\" "
                               state-num-str))))
    (if (eq statep (emisc--touchpad-enabled-p))
        (message (concat "Touchpad " state-desc-str))
      (message (concat "Touchpad could not be" state-desc-str)))))

(defun emisc--touchpad-enabled-p ()
  (let (matches)
    (dotimes (id-str-ind (length emisc--touchpad-id-strs))
      (add-to-list 'matches
                   (string-match "1\n"
                                 (shell-command-to-string
                                  (concat "xinput list-props "
                                          (nth id-str-ind emisc--touchpad-id-strs)
                                          " | grep \"Device Enabled\"")))))
    (not (member nil matches))))

(defun emisc--touchpad-id-strs ()
  (let* ((ind 0)
         (xinput-string (shell-command-to-string "xinput"))
         (re-string (concat emisc-touchpad-str ".*id=\\([0-9]*\\)"))
         (id-strs '()))
    (while (string-match re-string xinput-string ind)
      (setq ind (match-end 0))
      (add-to-list 'id-strs (match-string 1 xinput-string)))
    id-strs))

(defun emisc-split-window-above ()
  (interactive)
  (split-window-below)
  (windmove-down))

(defun emisc-split-window-left ()
  (interactive)
  (split-window-right)
  (windmove-right))

;;; generic registers

(defun emisc-store-register ()
  "Generic register storage function for strings and points."
  (interactive)
  (if (region-active-p)
      (call-interactively 'copy-to-register)
    (call-interactively 'point-to-register)))

(defun emisc-recall-register (register &optional arg)
  "Generic register recall function for strings and points."
  (interactive "cRecall register: \nP")
  (condition-case nil
      (jump-to-register register)
    (error
     ;; The contents of register are not a point. Assume they are a
     ;; string and insert them. Also, flip the standard meaning of the
     ;; prefix arg; if the arg is passed, leave point before the
     ;; inserted string, otherwise leave it at the end.
     (insert-register register (if arg nil t)))))

;;; python

(defun emisc-transpose-py-arg-back ()
  "Transpose the current Python list/func/dict item with the previous one."
  (interactive)
  (emisc-transpose-py-arg t))

(defun emisc-transpose-py-arg (&optional backwards)
  "Transpose the current Python list/func/dict item with the next one."
  (interactive)
  (let* ((start-marker (point-marker))
         (left-pt (progn (python-nav-backward-up-list) (point)))
         (right-pt (progn (forward-sexp) (point)))
         (left-bounds nil)
         (right-bounds nil)
         (found nil)
         (delim-ind 0)
         (cur-arg-ind nil)
         (next-arg-ind nil))

    ;; Collect all left boundaries of the Python arguments, including
    ;; just inside the open paren of the containing sexp (i.e. the one
    ;; determined by left-pt and right-pt).

    (goto-char left-pt)
    (setq left-bounds (append left-bounds (list (1+ left-pt))))
    (while (search-forward-regexp ",[ \n]*[^ \n]" right-pt t)
      (backward-char) ; Since Elisp has no lookaheads, we'll just move back.
      ;; Determine whether point is directly in the containing sexp.
      (when (eq left-pt (save-excursion (python-nav-backward-up-list) (point)))
        (setq left-bounds (append left-bounds (list (point))))))

    ;; Collect all right boundaries of the Python arguments, including
    ;; just inside the close paren of the containing sexp.
    (goto-char right-pt)
    (setq right-bounds (append right-bounds (list (1- right-pt))))
    (while (search-backward-regexp "," left-pt t)
      ;; Determine whether point is directly in the containing sexp.
      (when (eq left-pt (save-excursion (python-nav-backward-up-list) (point)))
        (setq right-bounds (append right-bounds (list (point))))))
    (setq right-bounds (reverse right-bounds))

    ;; Find the index of the nearest left-boundary forward from
    ;; start-marker, and set left and right start indices based on it.
    (while (not next-arg-ind)
      (cond
       ((>= (nth delim-ind left-bounds) start-marker)
        (setq next-arg-ind delim-ind))
       ((eq delim-ind (1- (length left-bounds)))
        ;; The start-marker is after (or in the middle of) the last
        ;; arg, so wrap next-arg-ind around to the first arg,
        (setq next-arg-ind 0)))
      (setq delim-ind (1+ delim-ind)))

    (setq cur-arg-ind (if (= next-arg-ind 0)
                          (1- (length left-bounds)) ; wrap around
                        (1- next-arg-ind)))

    ;; Shift next-arg-ind backwards by 2 depending upon the value of BACKWARDS.
    (when backwards
      (setq next-arg-ind (- next-arg-ind 2))
      (when (< next-arg-ind 0 )
        (setq next-arg-ind (+ next-arg-ind (length left-bounds)))))

    (let ((cur-left (nth cur-arg-ind left-bounds))
          (cur-right (nth cur-arg-ind right-bounds))
          (next-left (nth next-arg-ind left-bounds))
          (next-right (nth next-arg-ind right-bounds)))
      ;; Transpose the current arg with the next arg.
      (transpose-regions cur-left cur-right next-left next-right)
      ;; Put point right after cur-arg.
      (goto-char (if (> cur-left next-left)
                     (+ next-left (- cur-right cur-left))
                   next-right)))))

;;; keyboard

(defcustom emisc-keyboard-layout 'qwerty
  "Current keyboard layout.

Determines layout-specific bindings. Mainly affects commands such
as ace-window, which tries to prioritize home-row keys for speed."
  :type
  '(choice
    (const qwerty)
    (const colemak))
  :group 'emisc)

;;; scratch

(defcustom emisc-scratch-template '("~/scratch" "scratch")
  "Location and filename pattern for emisc scratch files."
  :group 'emisc
  :type '(list (string :tag "Directory")
               (string :tag "Base")))

(define-arx emisc-scratch-rx
  '((dir (* (regexp "[[:alnum:]/]")))
    (base (+ (in alnum)))
    (fnum (+ (in digit)))
    (ext (+ (in alnum)))
    (fname (and (group base)
                "_"
                (group fnum)
                "\."
                (group ext)))
    (fpath
     (and
      (group dir)
      "/"
      (group fname)))))

(defun emisc--scratch-mru-num (ext)
  "Return the number of the latest scratch file with extension EXT."
  (let* ((dir (expand-file-name (nth 0 emisc-scratch-template)))
         (base (nth 1 emisc-scratch-template))
         (nums (loop for x in (directory-files dir)
                     with parsed
                     do (setq parsed (emisc--scratch-parse-path (f-join dir x)))
                     when (and parsed
                               (string-equal ext (nth 4 parsed)))
                     collect (string-to-number (nth 3 parsed)))))
    (when nums
      (apply 'max nums))))

(defun emisc--scratch-str (ext num)
  "Convert an extension and number to a file path."
  (let ((dir (expand-file-name (nth 0 emisc-scratch-template)))
        (base (nth 1 emisc-scratch-template)))
    (f-join dir (concat base "_" (number-to-string num )"." ext))))

(defun emisc--scratch-parse-path (path)
  "Return the components of the scratch path or nil if it is invalid."
  (when (string-match (emisc-scratch-rx fpath) path)
    (let ((dir (match-string 1 path))
          (fname (match-string 2 path))
          (base (match-string 3 path))
          (fnum (match-string 4 path))
          (ext (match-string 5 path)))
      (when (and (string-equal dir (expand-file-name
                                    (nth 0 emisc-scratch-template)))
                 (string-equal base (nth 1 emisc-scratch-template)))
        (list dir fname base fnum ext)))))

(defun emisc--scratch-ext ()
  "Get the current value of the extension as set in the hydra."
  (cond
   (hydradio-emisc-scratch/bash "sh")
   (hydradio-emisc-scratch/elisp "el")
   (hydradio-emisc-scratch/go "go")
   (hydradio-emisc-scratch/org "org")
   (hydradio-emisc-scratch/python "py")
   (hydradio-emisc-scratch/txt "txt")
   (t "txt")))

(defun emisc--scratch-cycle (next-p)
  "Select the previous scratch file (or next if NEXT-P is non-nil)."
  (let ((parsed (emisc--scratch-parse-path (buffer-file-name)))
        (cur-ext (emisc--scratch-ext)))
    (unless parsed
      (error "Current buffer is not a scratch file"))
    (destructuring-bind (_ _ _ fnum ext) parsed
      (unless (string-equal cur-ext ext)
        (error "Buffer extension does not match"))
      (let* ((target-num (if next-p
                             (1+ (string-to-number fnum))
                           (1- (string-to-number fnum))))
             (mru-num (emisc--scratch-mru-num ext))
             (rectified-num (cond
                             ((< target-num 0)
                              0)
                             ((> target-num mru-num)
                              mru-num)
                             (t target-num)))
             (target-filename (emisc--scratch-str ext rectified-num)))
        (if (file-exists-p target-filename)
            (progn
              (kill-buffer-if-not-modified (current-buffer))
              (find-file target-filename))
          (error (format "Missing scratch buffer %s" target-filename)))))))

(defun emisc--scratch-hydra-mru ()
  "Select the latest scratch file with the currently selected extension."
  (interactive)
  (let* ((ext (emisc--scratch-ext))
         (num (emisc--scratch-mru-num ext)))
    (if num
        (find-file (emisc--scratch-str ext num))
      (error "No existing scratch file"))))

(defun emisc--scratch-hydra-create ()
  "Create a new scratch file with the currently selected extension."
  (interactive)
  (let* ((ext (emisc--scratch-ext))
         (num (emisc--scratch-mru-num ext)))
    (find-file (emisc--scratch-str ext (if num
                                           (1+ num)
                                         0)))
    (save-buffer)))

(defun emisc--scratch-hydra-prev ()
  (interactive)
  (emisc--scratch-cycle nil))

(defun emisc--scratch-hydra-next ()
  (interactive)
  (emisc--scratch-cycle t))

(defhydradio hydradio-emisc-scratch ()
  (bash "_b_ bash:")
  (elisp "_e_ elisp:")
  (go "_g_ go:")
  (org "_o_ org:")
  (python "_p_ python:")
  (txt "_t_ txt:"))

(defhydra hydra-emisc-scratch (:foreign-keys run :hint nil)
  (concat (hydra--table hydradio-emisc-scratch/names 7 2
                        '("  % -12s %% -3`%s"
                          "%s %%`%s"))
          "")
  ("b" (progn
         (hydra-reset-radios hydradio-emisc-scratch/names)
         (hydradio-emisc-scratch/bash)))
  ("e" (progn
         (hydra-reset-radios hydradio-emisc-scratch/names)
         (hydradio-emisc-scratch/elisp)))
  ("g" (progn
         (hydra-reset-radios hydradio-emisc-scratch/names)
         (hydradio-emisc-scratch/go)))
  ("o" (progn
         (hydra-reset-radios hydradio-emisc-scratch/names)
         (hydradio-emisc-scratch/org)))
  ("p" (progn
         (hydra-reset-radios hydradio-emisc-scratch/names)
         (hydradio-emisc-scratch/python)))
  ("t" (progn
         (hydra-reset-radios hydradio-emisc-scratch/names)
         (hydradio-emisc-scratch/txt)))
  ("c" (emisc--scratch-hydra-create) "create" :color blue)
  ("m" emisc--scratch-hydra-mru "mru" :color red)
  ("C-p" (emisc--scratch-hydra-prev) "prev" :color red)
  ("C-n" (emisc--scratch-hydra-next) "next" :color red)
  ("q" nil "cancel"))

;;; screenshot

(defcustom emisc-screenshot-template '("~/screenshots" "pic" "png")
  "Location and filename pattern for emisc screenshot files."
  :group 'emisc
  :type '(list (string :tag "Directory")
               (string :tag "Base")
               (string :tag "Extension")))

(defun emisc-gnome-screenshot (prefix)
  "Run gnome-screenshot."
  (interactive "P")
  (let ((touchpad-on-p (emisc--touchpad-enabled-p)))
    ;; Ensure touchpad is on for the duration of the command.
    (unless touchpad-on-p
      (emisc-enable-touchpad))
    (destructuring-bind (dir base ext) emisc-screenshot-template
      (cond
       ((null prefix)
        (call-process-shell-command
           (concat "gnome-screenshot -ac &")))
       ((and (integerp prefix))
        (mkdir dir t)
        (let ((path (emisc-available-fname dir (concat base "." ext))))
          (call-process-shell-command
           (concat "gnome-screenshot -a --file=" (f-join dir path) "&"))))
       (t
        (call-process-shell-command "gnome-screenshot -i &"))))
    ;; Restore touchpad to its original state.
    (unless touchpad-on-p
      (emisc-disable-touchpad))))

;;; exwm

(defun emisc-query-exwm-props ()
  "List properties of the current exwm buffer."
  (interactive)
  (let ((props '(exwm--desktop exwm-window-type exwm-class-name
                               exwm-instance-name exwm-title
                               exwm--title-is-utf8 exwm-transient-for
                               exwm--protocols exwm-state exwm--ewmh-state))
        (str ""))
    (dolist (prop props str)
      (setq str (concat str
                        (format "%S" prop)
                        ": "
                        (format "%S" (eval prop))
                        "\n")))
    (message str)))

(defun emisc-exwm-restart ()
  "Restart EXWM, ensuring that an env var is set iff doing so.

If the var is set here, then calling `exwm-restart' but saying
'n' to y-or-n-p can leave the env var set, even though Emacs is
not restarting. So instead we add it to the end of
`kill-emacs-hook'."
  (interactive)
  (add-hook 'kill-emacs-hook
            (lambda () (setenv emisc-exwm-restart-env ""))
            t)
  (exwm-restart))

(defvar emisc-exwm-testing-env "TESTING_EXWM"
  "Environment variable read by init.el to determine init is being tested.")

(defun emisc--exwm-report-init ()
  "Used by the non-EXWM test instance of Emacs to report init status."
  ;; Make a buffer listing all mismatched keybindings.
  (let ((mismatches (emisc-scan-bindings-mismatches)))
    (when mismatches
      (switch-to-buffer (generate-new-buffer "*key-binding-mismatches*"))
      (insert (string-join mismatches "\n"))))

  ;; Gather the contents of various startup buffers into a report buffer.
  (let ((headers '("*straight-process*" "*Messages*" "*Warnings*"
                   "*Compile-Log*" "*key-binding-mismatches*"))
        (buffer (generate-new-buffer "*init report*")))
    (switch-to-buffer buffer)
    (dolist (header headers)
      (when (get-buffer header)
        (insert (propertize (concat header "\n") 'face '(:background "black")))
        (insert (with-current-buffer header
                  (buffer-string)))
        (insert "\n")))
    (delete-other-windows)))

(defun emisc-exwm-test-init ()
  "Tests Emacs startup by starting a non-EXWM test instance of Emacs."
  (interactive)
  (setenv emisc-exwm-testing-env "")
  (call-process-shell-command "emacs"
                              nil
                              nil
                              nil
                              "--eval=\"(emisc--exwm-report-init)\""
                              "&")
  (setenv emisc-exwm-testing-env nil))

(defun emisc-exwm-kill-buffers ()
  "Kill all EXWM buffers."
  (interactive)
  (let ((exwm-buffers (loop for buf in (buffer-list)
                            when (with-current-buffer buf
                                   exwm--id)
                            collect buf)))
    (loop for buf in exwm-buffers
          do (kill-buffer buf))))

(defun emisc-exwm-randr-refresh ()
  (dolist (window (window-list-1 nil 0 t))
    ;; Bury EXWM buffers on startup.
    (select-window window)
    (when exwm--id
      (bury-buffer)))
  ;; Sometimes the lettering from `ace-window-display-mode' gets messed up
  ;; after randr does its thing (although `ace-window-mode' itself is
  ;; unaffected) so force an update.
  (aw-update))

;;; kill emacs/os

(defun emisc-kill-emacs ()
  "Same as standard C-x C-c but restarts EXWM instead if active."
  (interactive)
  (if (frame-parameter (selected-frame) 'exwm-active)
      (call-interactively 'emisc-exwm-restart)
    (call-interactively 'save-buffers-kill-terminal)))

(defun emisc-force-kill-emacs ()
  "When C-x C-c i.e. save-buffers-kill-terminal won't work, this will."
  (interactive)
  (let ((kill-emacs-hook nil))
    (kill-emacs)))

(defun emisc-shutdown ()
  "Issue an immediate shutdown."
  (interactive)
  (savehist-save)
  (when (y-or-n-p "Shutdown immediately? ")
    (start-process "emacs-shutdown-process" "*shutdown*" "sudo" "shutdown" "0")))

(defun emisc-reboot ()
  "Issue an immediate reboot."
  (interactive)
  (savehist-save)
  (when (y-or-n-p "Reboot immediately? ")
    (start-process "emacs-reboot-process" "*reboot*" "reboot")))

(defhydra hydra-emisc-exit (:color blue :hint nil)
  ("k" emisc-kill-emacs "kill")
  ("f" emisc-force-kill-emacs "force-kill")
  ("s" emisc-shutdown "shutdown")
  ("r" emisc-reboot "reboot")
  ("q" nil "quit"))

;;; ace-window

;; In the below, swapping means that the source window ends up containing the
;; target buffer, whereas uncover means that `other-buffer' is revealed there
;; instead. Returning means that point ends up in the same window in which it
;; started.
;; 
;; The uncover functionality is for this use case. You need to open a dired on
;; a remote machine, so you move point onto a window showing a shell for that
;; machine to establish the proper `default-directory'. Then you C-x C-f and
;; select the file, but now it has buried your shell which perhaps you need to
;; take output from. So without these functions you would 1) swap the buffer to
;; the target destination, 2) swap back to the window where you want your
;; shell, 3) switch to shell buffer, and 3) optionally return to the dired
;; buffer you swapped away. With these functions you would 1) uncover the shell
;; by moving the source buffer to the target window, with point either
;; remaining or moving with it.

(defun emisc-aw-swap (window)
  (interactive)
  (emisc--aw-swap-generic window nil nil))

(defun emisc-aw-swap-and-return (window)
  (interactive)
  (emisc--aw-swap-generic window nil t))

(defun emisc-aw-uncover (window)
  (interactive)
  (emisc--aw-swap-generic window t nil))

(defun emisc-aw-uncover-and-return (window)
  (interactive)
  (emisc--aw-swap-generic window t t))

(defun emisc--aw-swap-generic (window uncoverp returnp)
  (let ((start (selected-window)))
    (let ((aw-swap-invert returnp))
      (aw-swap-window window)
      (save-selected-window
        (select-window start)
        (when git-gutter-mode
          (git-gutter))
        (select-window window)
        (when git-gutter-mode
          (git-gutter))))
    (when uncoverp
      (save-selected-window
        (select-window start)
        (switch-to-buffer nil)))))

(defun emisc-ace-ediff ()
  (interactive)
  (let (win1 win2)
    (message "Select first window")
    (setq win1 (aw-select "AW - Select"))
    (message "Select second window")
    (setq win2 (aw-select "AW - Select"))
    (when (eq win1 win2)
      (error "Cannot select same window."))
    ;; There are `ediff-windows-linewise' and `ediff-windows-wordwise' but they
    ;; seem to work strangely.
    (ediff-buffers (window-buffer win1) (window-buffer win2))))

;;; browser

(defcustom emisc-firefox-urls '("http://duckduckgo.com")
      "List of urls to be opened by `emisc-firefox-startup'"
      :type '(repeat string)
      :group 'emisc)

(defun emisc-browser-startup ()
  (interactive)
  (let* ((num-browser-buffers (length (loop for x in (buffer-list)
                                            if (s-starts-with-p
                                                "(" (buffer-name x))
                                            collect (buffer-name x))))
         (few-buffers-p (> 5 num-browser-buffers)))
    (when (or few-buffers-p
              (y-or-n-p (format "There are %d browser buffers. Are you sure?"
                                num-browser-buffers)))
      (emisc-firefox-startup) )))

(defun emisc-firefox-startup ()
  (interactive)
  ;; Start Firefox with no-hup so that if Emacs is restarted, Firefox doesn't
  ;; die. This enables restarting Emacs with EXWM without having to restart
  ;; the browsers.
  (call-process-shell-command (concat "nohup firefox"
                                      (loop for url in emisc-firefox-urls
                                            concat " --new-window "
                                            concat url)
                                      " > /dev/null &")))

;;; magit

(defun emisc-magit-display-buffer (buffer)
  "Display magit buffers by 1) selecting visible, or 2) by using aw-select."
  (if (eq (length (window-list-1 nil nil t)) 1)
      (magit-display-buffer-traditional buffer)
    ;; Suppress fallback display actions, which allows display-buffer to just
    ;; return nil if it cannot reuse a window already showing the buffer.
    (let* ((display-buffer-fallback-action nil)
           (reused-win (display-buffer buffer '(display-buffer-reuse-window) t)))
      (if reused-win
          reused-win
        (save-selected-window
          (select-window (aw-select "AW - Select"))
          (display-buffer buffer '(display-buffer-same-window) t))))))

(defun emisc-magit-section-forward-sibling ()
  (interactive)
  (magit-section-forward-sibling)
  (recenter-top-bottom 0))

(defun emisc-magit-section-backward-sibling ()
  (interactive)
  (magit-section-backward-sibling)
  (recenter-top-bottom 0))

(defun emisc-magit-pull-master ()
  (interactive)
  (magit-run-git-async "pull" nil (magit-get-upstream-remote "master") "master"))

;;; scan bindings

(define-arx emisc-scan-bindings-re
  '((ws (regexp "[ ]*"))
    (varname (+ (regexp "[[:alnum:]-:/]")))
    (ctrl (regexp "C-"))
    (meta (regexp "M-"))
    (shift (regexp "S-"))
    (super (regexp "s-"))
    (mod (or ctrl meta shift super))
    (keyseq (and (+ mod) (group (regexp "."))))
    (binding (and  "(\"" (group keyseq) "\"" ws "\." ws (group varname) ")"))))

(defun emisc-scan-bindings-init ()
  "Return a sorted list of bindings found in init.el."
  (with-temp-buffer 
    (insert-file-contents user-init-file)
    (let ((start 0)
          (str (buffer-string))
          (case-fold-search nil)
          matches)
      (while (string-match (emisc-scan-bindings-re binding) str start)
        (setq start (match-end 0))
        (push `(,(match-string 1 str) . ,(intern (match-string 3 str))) matches))
      (sort matches #'emisc--scan-bindings-compare))))

(defun emisc--scan-bindings-compare (binding0 binding1)
  "Compare two keybindings to generate a proper ordering."
  (let ((str0 (car binding0))
        (str1 (car binding1))
        (case-fold-search nil)
        (mod-res `(,(emisc-scan-bindings-re ctrl)
                   ,(emisc-scan-bindings-re meta)
                   ,(emisc-scan-bindings-re shift)
                   ,(emisc-scan-bindings-re super))))
    (catch 'done
      (dolist (mod-re mod-res)
        (let ((match0 (string-match mod-re str0))
              (match1 (string-match mod-re str1)))
          (when (xor match0 match1)
            (throw 'done (eq nil match0)))))
      (string< (progn (string-match (emisc-scan-bindings-re keyseq) str0)
                      (match-string 1 str0))
               (progn (string-match (emisc-scan-bindings-re keyseq) str1)
                      (match-string 1 str1))))))

(defun emisc-scan-bindings-exwm ()
  "Return a sorted list of bindings found in `exwm-input-global-keys'."
  (sort (loop for x in exwm-input-global-keys
              collect `(,(key-description (car x)) . ,(cdr x)))
        #'emisc--scan-bindings-compare))

(defun emisc-scan-bindings-mismatches ()
  "Return a list of EXWM bindings with no init.el counterpart."
  (let ((exwm-bindings (emisc-scan-bindings-exwm))
        (init-bindings (emisc-scan-bindings-init))
        mismatches)
    (dolist (exwm-binding exwm-bindings)
      (let ((init-binding (assoc (car exwm-binding) init-bindings)))
        (when (and init-binding
                   (not (eq (cdr exwm-binding) (cdr init-binding))))
          (push (concat "for "
                        (car exwm-binding)
                        " exwm has: "
                        (symbol-name (cdr exwm-binding))
                        ", init has: "
                        (symbol-name (cdr init-binding))) mismatches))))
    mismatches))

(defun emisc-scan-bindings-show ()
  "Display a list of all keybindings found in init.el."
  (interactive)
  (switch-to-buffer "*key bindings*")
  (delete-region (point-min) (point-max))
  (dolist (init-binding (emisc-scan-bindings-init))
    (let* ((keyseq (car init-binding))
           (cmd (cdr init-binding))
           (ws (string-join (loop repeat (- 8 (length keyseq))
                                  collect " "))))
      
      (insert (concat keyseq ws ": " (symbol-name cmd) "\n"))))
  (let ((mismatches (emisc-scan-bindings-mismatches)))
    (when mismatches
      (insert (concat "\n\n" (string-join mismatches "\n"))))))

;;; eww

(defun emisc-start-eww (&optional rename-uniquely-p)
  (interactive "P")
  (when (and rename-uniquely-p
             (member "*eww*" (loop for x in (buffer-list)
                                   collect (buffer-name x))))
    (with-current-buffer "*eww*"
      (rename-uniquely)))
  (with-temp-buffer
    ;; If selected buffer, is, say *eww<2>*, it will be reused, so select a
    ;; temporary buffer instead, ensuring that (regardless of rename-uniquely)
    ;; the new eww buffer will be *eww*.
    (call-interactively 'eww)))

(defun emisc-start-eww-uniquely ()
  (interactive)
  (emisc-start-eww t))

(defun emisc-eww-browse-external ()
  (interactive)
  (eww-browse-with-external-browser (plist-get eww-data :url)))

;;; blink

(defface emisc-blink-face
  '((t :background "#ffff8c8c0000"))
  "Default face for emisc-blink."
  :group 'emisc)

(defvar emisc--blink-ovs nil
  "Overlays for emisc-blink.")

(defun emisc-blink ()
  "Blink the selected line or region.

Since the region may be non-contiguous we have to iterate through
the pairs. This was not checked for consequences of overlapping pairs."
  (interactive)
  (let* ((ranges (if (region-active-p)
                     (region-bounds)
                   `((,(point-at-bol) . ,(line-beginning-position 2)))))
         (ovs (loop for range in ranges
                    collecting (make-overlay (car range)
                                             (cdr range)))))
    ;; Set up overlays
    (loop for ov in ovs
          do
          (overlay-put ov 'emisc-blink t)
          (overlay-put ov 'priority most-positive-fixnum)
          (overlay-put ov 'window (selected-window)))

    ;; Animate
    (mapcar (lambda (x) (overlay-put x 'face 'emisc-blink-face)) ovs)
    (sit-for 0.1)
    (mapcar (lambda (x) (overlay-put x 'face nil)) ovs)
    (sit-for 0.1)
    (mapcar (lambda (x) (overlay-put x 'face 'emisc-blink-face)) ovs)
    (sit-for 0.1)

    ;; Tear down overlays
    (loop for ov in ovs
          do          
          (delete-overlay ov))))

;;; savehist registers

(defvar emisc-register-alist nil
  "Printable proxy for `register-alist', intended to be added to
`savehist-additional-variables'.")

(defun emisc-dump-registers ()
  "Store the printable values of `register-alist' in
`emisc-register-alist'."
  (setq emisc-register-alist
        (remove-if-not (lambda (x)
                         (savehist-printable
                          (cdr (assoc (car x) register-alist))))
                       register-alist)))

(defun emisc-load-registers ()
  (setq register-alist emisc-register-alist))

;;; brainstack

(defvar emisc-brainstack '())

(defun emisc-push-brainstack (str)
  (interactive "s: ")
  (push str emisc-brainstack))

(defun emisc-pop-brainstack ()
  (interactive)
  (when emisc-brainstack
    (pop emisc-brainstack))
  (emisc-view-brainstack))

(defun emisc-view-brainstack ()
  (interactive)
  (if emisc-brainstack
      (message (string-join emisc-brainstack "\n"))
    (message "*empty*")))

;;; reminderers

(defvar emisc--reminderers nil
  "List of active reminderers, in order of recency of creation or firing.")

(defvar emisc--reminderer-mutex (make-mutex)
  "Required to be held during changes to emisc--reminderers.")

(defun emisc-reminderer (note interval)
  "Register a reminder timer to echo NOTE every INTERVAL seconds.

The reminder puts itself at the front of `emisc-reminderers' on
creation and firing."
  (interactive "sTimer note: \nnSeconds: ") 
  (let* ((timer (timer-create))
         (first-time (timer-relative-time nil interval))
         (reminderer (list timer note)))
    (timer-set-time timer first-time interval)
    (push reminderer emisc--reminderers)
    (timer-set-function timer
                        (lambda ()
                          ;; Move this reminderer to the front of the list.
                          (with-mutex emisc--reminderer-mutex
                            (setq emisc--reminderers
                                  (-remove-item reminderer emisc--reminderers))
                            (push reminderer emisc--reminderers))
                          (message note)))
    (timer-activate timer)))

(defun emisc-reminderer-delete-recent ()
  "Delete the most recent reminderer to fire or be created."
  (interactive)
  (if emisc--reminderers
      (emisc--reminderer-delete (car emisc--reminderers))
    (message "No reminderers to delete.")))

(defun emisc--reminderer-delete (reminderer)
  "Delete a particular reminderer."
  (with-mutex emisc--reminderer-mutex
    (cancel-timer (car reminderer))
    (setq emisc--reminderers
          (-remove-item reminderer emisc--reminderers))))

(defun emisc-reminderer-delete ()
  "List active reminderers."
  (interactive)
  (ivy-read "Delete reminderer: "
            (mapcar 'cadr emisc--reminderers)
            :action (lambda (x)
                      (emisc--reminderer-delete
                       (-first (lambda (y) (equal x (cadr y)))
                               emisc--reminderers)))
            :require-match t))

(defhydra hydra-emisc-reminderer (:color blue :hint nil)
  ("c" emisc-reminderer "create")
  ("r" emisc-reminderer-delete-recent "delete recent")
  ("d" emisc-reminderer-delete "delete from list")
  ("q" nil "cancel"))

;;; misc

(defun emisc-kill-ring-save-appendable (beg end)
  "Like kill-ring-save, but sets `this-command' to `kill-region'.

This allows whole-line-or-region to adapt this command for
multi-line copying with successive calls, just as successive
calls to `kill-region' result in appending kills rather than
creating new ones."
  (interactive "r")
  (kill-ring-save beg end)
  ;; Symbol `kill-region' is special. See `last-command' for more.
  (setq this-command 'kill-region))

(defun emisc-wlr-kill-ring-save-appendable (prefix)
  "Whole-line-or-region function for multi-line copying.

This is superior to `whole-line-or-region-kill-ring-save' in that
it moves point down a line and is appendable. Thus successive
\\[emisc-wlr-kill-ring-save-appendable] commands result in one
kill of multiple lines."
  (interactive "p")
  (if (region-active-p)
      (kill-ring-save (point) (mark))
    (whole-line-or-region-call-with-region
     'emisc-kill-ring-save-appendable prefix t)
    (move-beginning-of-line 2)))

(defun emisc-swap-two-marked-regions nil
  "Swap regions defined by point, mark and recent entries of mark-ring.

So to swap Jim with Bob in

dictionary['Jim']['Bob']

one would put point before the J, then set mark there, put point
after the m and set mark there, put point before B and set mark
there, and finally put point after b and call this function"
  (interactive)
  (when (>= (length mark-ring) 4)
    (let* ((sorted-points (sort `(,(marker-position (nth 0 mark-ring))
                                  ,(marker-position (nth 1 mark-ring))
                                  ,(mark) ,(point)) '<))
           (region-1-string (buffer-substring (nth 0 sorted-points)
                                              (nth 1 sorted-points)))
           (region-2-string (buffer-substring (nth 2 sorted-points)
                                              (nth 3 sorted-points))))
      (save-excursion
        (delete-region (nth 2 sorted-points) (nth 3 sorted-points))
        (goto-char (nth 2 sorted-points))
        (insert region-1-string)
        (delete-region (nth 0 sorted-points) (nth 1 sorted-points))
        (goto-char (nth 0 sorted-points))
        (insert region-2-string)))))

(defun emisc-qrr-swap-opposites ()
  "Begin a query-replace for swapping pairs of opposites.

See `emisc-qrr-opposites-alist' for the pairs that are searched."
  (interactive)
  (let* ((alist emisc-qrr-opposites-alist)
         (opposites (loop for elem in alist
                          collect (car elem)
                          collect (cdr elem)))
         (joined-str (mapconcat 'identity opposites "\\b\\)\\|\\(\\b"))
         (regex-str (concat "\\(\\(\\b" joined-str "\\b\\)\\)"))
         (repl-str '(replace-eval-replacement
                     replace-quote
                     (or (cdr (assoc (match-string 1) alist))
                         (car (rassoc (match-string 1) alist))))))
    (query-replace-regexp regex-str repl-str)))

(defun emisc-notebooklist-open ()
  "Call `ein:notebooklist-open' with some extra setup.

This 1) forces a refresh of kernelspecs in case a new one has
been added, and 2) starts the Jupyter Notebook Server."
  (interactive)
  (when (boundp 'ein:available-kernelspecs)
    (clrhash ein:available-kernelspecs))
  (async-shell-command (concat "jupyter notebook --port 8888 --no-browser "
                               "--notebook-dir=~/ --NotebookApp.token=''")
                       "*Jupyter Notebook Server*")
  (call-interactively 'ein:notebooklist-open))

(defun emisc-toggle-clm ()
  "Toggle command-log-mode.

I want to hit one key sequence to activate CLM globally and show
the output buffer, then hit the same key sequence to deactivate
it and close the buffer. The `clm/toggle-command-log-buffer'
command works as expected with
`command-log-mode-key-binding-open-log' and
`command-log-mode-is-global' set to t, except that deactivates
the global minor mode, not the buffer-local

"
  (interactive)
  (let ((global-active-p global-command-log-mode))
    (clm/toggle-command-log-buffer)
    (when global-active-p
      (global-command-log-mode -1))))

(defun emisc-copy-bp-str (prefix)
  "Add the golang breakpoint linespec at point to a register."
  (interactive "P")
  (let* ((path (file-local-name (file-truename (buffer-file-name))))
         (line-no (line-number-at-pos nil t))
         (bp-str (concat "b " path ":" (number-to-string line-no))))
    (if prefix
        (progn
          (message "Line killed: \"%s\"" bp-str)
          (kill-new bp-str))
      (set-register ?t bp-str)
      (message "Line saved to register: t \"%s\"" bp-str))))

(defun emisc-flip-bool (variable)
  "Flip the boolean value of VARIABLE, which must be a customizable boolean."
  (interactive
   (let ((v (variable-at-point))
         val)
     (setq val (completing-read (if (symbolp v)
                                    (format
                                     "Flip (%s is %s): " v (symbol-value v))
                                  "Flip: ")
                                obarray
                                (lambda (x)
                                  (and (boundp x)
                                       (eq (custom-guess-type x) 'boolean)))
                                t nil nil
                                (if (symbolp v) (symbol-name v))))
     (list (if (equal val "")
               v (intern val)))))
  (set variable (not (symbol-value variable)))
  (message "%s is now %s" variable (symbol-value variable)))

(defun emisc-multishell-complete ()
  (interactive)
  ;; Only allow matches to things with a / in the name. This prevents, e.g. "l"
  ;; getting stored in `multishell-history', but still allows on-the fly
  ;; creation of new shells.
  (let ((completion-regexp-list '(".*/.*")))
    (multishell-pop-to-shell 4 nil t)))

(defun emisc-sym-magit-status ()
  "Like magit-status, but follow symlinks first. Intended for "
  (interactive)
  (magit-status-internal (file-name-directory
                          (file-truename
                           buffer-file-name))))

(defcustom emisc-qrr-opposites-alist '(("add" . "remove")
                                       ("enable" . "disable")
                                       ("enabled" . "disabled")
                                       ("expand" . "contract")
                                       ("forward" . "backward")
                                       ("in" . "out")
                                       ("install" . "uninstall")
                                       ("next". "prev")
                                       ("on" . "off")
                                       ("open" . "close")
                                       ("pass" . "fail")
                                       ("passed" . "failed")
                                       ("read" . "write")
                                       ("right" . "left")
                                       ("save" . "load")
                                       ("store" . "recall")
                                       ("t" . "nil")
                                       ("true" . "false")
                                       ("up" . "down")
                                       ("yes" . "no"))
  "Pairs of opposites to be used in `emisc-qrr-swap-opposites'"
  :type 'alist
  :group 'emisc)

(defun emisc-instakill-buffer ()
  "Kill current buffer w/o prompts."
  (interactive)
  (kill-buffer (current-buffer)))

(defun emisc-available-fname (dir template)
  "Get the next available filename like TEMPLATE in DIR.

DIR should be a path to a directory, and TEMPLATE should satisfy

\([[:alnum:]_-]+\)_\([0-9]+\)\.\([[:alnum:]]+\)

where group 1 is the base, group 2 is the file number, and group
3 is the extension."
  (let ((max-num -1)
        (base (file-name-sans-extension template))
        (ext (file-name-extension template)))
    (dolist (fname (directory-files dir))
      (when (string-match (concat base
                                  "_\\([0-9]+\\)\\."
                                  ext)
                          fname)
        (let ((cur-num (string-to-number (match-string 1 fname))))
          (when (> cur-num max-num)
            (setq max-num cur-num)))))
    (concat base
            "_"
            (number-to-string (1+ max-num))
            "."
            ext)))

(defun emisc-counsel-locate-here ()
  "Run locate command, providing `default-directory' as initial input."
  (interactive)
  (counsel-locate (expand-file-name default-directory)))

(defun emisc-org-todo-toggle ()
  "Toggle between TODO and DONE, without needing to specify a state."
  (interactive)
  (if (org-entry-is-done-p)
      (org-todo 'todo)
    (org-todo 'done)))

(defun emisc-fill-paragraph ()
  (interactive)
  (if (eq major-mode 'org-mode)
      (org-fill-paragraph)
    (fill-paragraph))
  (set-window-hscroll nil 0))

(defun emisc-eval-previous-src-block ()
  (interactive)
  (save-excursion
    (org-babel-previous-src-block)
    (org-ctrl-c-ctrl-c)))

(defun emisc-org-tab-no-uncles ()
  (interactive)
  (save-excursion (org-overview))
  (org-show-set-visibility 'ancestors)
  (show-subtree))

(defun emisc-tail-log (prefix)
  "Complete for a host and app, then show a buffer tailing the log.

With prefix arg, tail stderr instead of stdout."
  (interactive "P")
  (let* ((log-path "/var/log")
         (chassis-app-names '("abacus" "arena" "cis" "miner"
                              "mscameraproxy" "nos" "palantir" "panda"
                              "pong" "preprocessor" "replicator"
                              "walker"))
         (shell-spec (let ((completion-regexp-list '(".*/.*")))
                       (multishell-read-unbracketed-entry "Select shell: " nil t)))
         (tfn (tramp-dissect-file-name
               (cadr (multishell-resolve-target-name-and-path shell-spec))))
         (tramp-log-dir (format "/%s:%s@%s:%s" (nth 1 tfn) (nth 2 tfn)
                                (nth 4 tfn) log-path))
         (app (completing-read "App: " (directory-files tramp-log-dir)
                               (lambda (x) (member x chassis-app-names)) t))
         (default-directory (f-join tramp-log-dir app))
         (log-fname (concat app "_" (if prefix
                                        "stderr"
                                      "stdout") ".log"))
         (buf (generate-new-buffer (concat "*tail " log-fname " on "
                                           (tramp-file-name-host tfn) "*"))))
    (start-file-process "log tail" buf "tail" "-f" log-fname)
    (pop-to-buffer buf)))

(define-arx emisc-origami-use-package-re
  '((po "(")
    (pc ")")
    (wc (regexp "\\w"))                 ; word constituent
    (ws (regexp "\\s-"))                ; whitespace
    (sc (regexp "\\s_"))                ; symbol constituent
    (ang (regexp ".*?"))                ; any non-greedy
    (st (regexp "[ \\t]"))              ; space tab
    (punc (regexp "[:?!]"))
    (prefix-orig (and "def" (* wc)))
    
    ;; This is strictly for reference and can be verified to match the original value of `origami-elisp-parser' and is
    ;; strictly for reference. 
    (orig-parser-re (and po
                         prefix-orig
                         (* ws)
                         (* (group (or sc wc punc)))
                         (? (group (and (* st) po ang pc)))))
    ;; This is the substitute which allows use-package forms to be folded.
    (new-parser-re (and po
                        (or (and (? "cl-") prefix-orig) "use-package")
                        (* ws)
                        (* (group (or sc wc punc)))
                        (? (group (and (* st) po ang pc)))))))

(defun emisc-elisp-parser (create)  
  (origami-lisp-parser create (emisc-origami-use-package-re new-parser-re)))

(defun emisc-switch-buffer ()
  "Switch to a non-multishell buffer with match required."
  (interactive)
  (setq this-command #'emisc-switch-buffer)
  (let ((ms-names (loop for entry in (multishell-list-entries)
                        collect (multishell-bracket
                                 (multishell-name-from-entry (car entry))))))
    
    ;; Instead of calling `internal-complete-buffer' directly we wrap it. This
    ;; is because `ivy-done' considers an `ivy-last' value of
    ;; `internal-complete-buffer' to be a special case and overrides
    ;; :require-match.
    (ivy-read "Switch to buffer: " (lambda (a b c)
                                     (internal-complete-buffer a b c))
              :require-match t
              :predicate (lambda (x) (not (member (car x) ms-names)))
              :keymap ivy-switch-buffer-map
              :preselect (buffer-name (other-buffer (current-buffer)))
              :action #'ivy--switch-buffer-action
              :matcher #'ivy--switch-buffer-matcher
              :caller 'ivy-switch-buffer)))

(defun emisc-counsel-bookmark ()
  "Like `counsel-bookmark', but match is required."
  (interactive)
  (ivy-read "Jump to bookmark: "
            (bookmark-all-names)
            :history 'bookmark-history
            :action (lambda (x)
                      (cond ((and counsel-bookmark-avoid-dired
                                  (member x (bookmark-all-names))
                                  (file-directory-p (bookmark-location x)))
                             (with-ivy-window
                               (let ((default-directory (bookmark-location x)))
                                 (counsel-find-file))))
                            ((member x (bookmark-all-names))
                             (with-ivy-window
                               (bookmark-jump x)))
                            (t
                             (bookmark-set x))))
            :caller 'counsel-bookmark
            :require-match t))

(defun emisc-yank-rectangle-dwim ()
  "Insert `killed-rectangle' normally, or if on a blank line, add newlines.

Intended for use with multiple-cursors, which sets
`killed-rectangle' after a copy command."
  (interactive)
  (when (and (bolp) (eolp))
    (save-excursion
      (insert (string-join (-repeat (length killed-rectangle) "\n")))))
  (yank-rectangle))

(defun emisc-dired-multi-windows ()
  "Replace current window with a window for each file marked in Dired buffer."
  (interactive)
  (when-let ((fpaths (dired-get-marked-files)))
    (find-file (pop fpaths))
    (while fpaths
      (condition-case nil (emisc-split-window-above)
        ;; If the window is too small for splitting, try balancing the window
        ;; column first.
        (error (emisc-autowiden-balance-wc)
               (emisc-split-window-above)))
      (find-file (pop fpaths)))
    (emisc-autowiden-balance-wc)))

(defun emisc-org-goto (prefix)
  "Call `counsel-outline' normally, or complete in steps if PREFIX is non-nil.

Ivy does not play well with `org-outline-path-complete-in-steps'
set to t, so use standard `org-goto' instead. "
  (interactive "P")
  (cond
   (prefix
    (let ((org-outline-path-complete-in-steps t)
          (ivy-mode-orig ivy-mode))
      (ivy-mode 0)
      (unwind-protect
          ;; If, say, `keyboard-quit' is called, make sure ivy-mode is still
          ;; restored.
          (org-goto)
        (when ivy-mode-orig
          (ivy-mode 1)))))
   ((fboundp 'counsel-outline)
    (call-interactively 'counsel-outline))
   (t
    (call-interactively 'org-goto))))

(defun emisc-org-up-heading ()
  "Like `outline-up-heading' but goes to parent if not on headline."
  (interactive)
  (if (org-at-heading-p)
      (outline-up-heading 1)
    (org-up-element)))

(defun emisc-org-cycle ()
  (let ((closedp (org-element-headline-parser (save-excursion (org-end-of-subtree) (point))))))
  (cond
   ((eq state 'folded)
    (ignore))
   ((eq state 'contents)
    (save-excursion (org-overview))
    (org-show-set-visibility 'ancestors)
    (show-subtree))
   ((eq state 'children)
    (save-excursion (org-overview))
    (org-show-set-visibility 'ancestors)
    (show-children))))

(defun emisc-browser ()
  "Open a blank instance of Firefox."
  (interactive)
  (call-process "gtk-launch" nil 0 nil "firefox_firefox.desktop"))

(defun emisc-kill-file-path ()
  "Add file name to kill-ring."
  (kill-new (buffer-file-name)))

(defun emisc-counsel-rg-go-projs ()
  ;; Search for all projects at top level that have a go.mod file with
  ;; ghe.iparadigms.com in the module name. The dir names cannot simply be
  ;; passed as EXTRA-ARGS to counsel-rg because they have to go after the
  ;; needle, i.e. the ivy input.
  (interactive)
  (let* ((dir-names
          (loop for fname in (directory-files "~/")
                ;; The first part of the and prevents Tramp from mounting
                ;; .tar.gz files.
                when (and (f-directory? (f-join "~/" fname))
                          (file-exists-p (f-join "~/" fname "go.mod"))
                          (with-temp-buffer
                            ;; insert enough bytes to capture the first line
                            (insert-file-contents (f-join "~/" fname "go.mod")
                                                  nil 0 200)
                            (search-forward
                             "module ghe.iparadigms.com/iParadigms/"
                             nil t nil)))
                concat (concat fname "/ ")))
         (counsel-rg-base-command
          (concat "rg -S --no-heading --line-number --color never -u %s "
                  dir-names)))
    (counsel-rg "" "~/")))

;; When `counsel-mode' is on, `counsel-yank-pop' redirects all `yank-pop' calls
;; to itself. If you want to call `yank-pop', just call this alias to it
;; instead.
(defalias 'emisc-yank-pop 'yank-pop)

(provide 'emisc-post)

;;; emisc-post.el ends here
