 ;;; emisc-tests.el --- Tests for emisc.el

;; Copyright (C) 2017 Richard Kinder

;; Author: Richard Kinder <kinderrich@gmail.com>
;; Version: 0.1
;; Keywords: emisc-tests
;; URL: https://bitbucket.org/richkinder/emisc.el

;;; Commentary:

;; Tests for emisc.el

;;; Code:

(require 'ert)
(add-to-list 'load-path ".")
(require 'emisc)
(eval-when-compile
  (require 'cl))

(ert-deftest emisc--py-for-read-cases ()
  (should (equal (emisc--py-for-read "for i in range(num):")
                 '("i" "num" nil nil "")))
  (should (equal (emisc--py-for-read "   for i in range(num): # some comment")
                 '("i" "num" nil nil " # some comment")))
  (should (equal (emisc--py-for-read "for x, y in zip(xs, ys):")
                 '(nil nil ("x" "y") ("xs" "ys") "")))
  (should (equal
           (emisc--py-for-read "for i, (x, y) in enumerate(zip(xs, ys)):")
           '("i" nil ("x" "y") ("xs" "ys") "")))
  (should (equal
           (emisc--py-for-read "for i, x in enumerate(xs):")
           '("i" nil ("x") ("xs") "")))
  (should (equal
           (emisc--py-for-read "for x in xs:")
           '(nil nil ("x") ("xs") "")))
  (should (equal (emisc--py-for-read "for x, y in zip(xs,\nys):")
                 '(nil nil ("x" "y") ("xs" "ys") "")))
  (should-error (emisc--py-for-read "for i in x, y:")))

(ert-deftest emisc--py-for-write-cases ()
  (should (equal (emisc--py-for-write '("i" "num" nil nil ""))
                 "for i in range(num):"))
  (should (equal (emisc--py-for-write '("i" "num" nil nil " # some comment"))
                 "for i in range(num): # some comment"))
  (should (equal (emisc--py-for-write '(nil nil ("x" "y") ("xs" "ys") ""))
                 "for x, y in zip(xs, ys):"))
  (should (equal
           (emisc--py-for-write '("i" nil ("x" "y") ("xs" "ys") ""))
           "for i, (x, y) in enumerate(zip(xs, ys)):"))
  (should (equal
           (emisc--py-for-write '("i" nil ("x") ("xs") ""))
           "for i, x in enumerate(xs):"))
  (should (equal
           (emisc--py-for-write '(nil nil ("x") ("xs") ""))
           "for x in xs:"))
  (should-error (emisc--py-for-write "for i in x, y:")))
