(define-package "emisc" "0.0.1"
  "Miscellaneous Emacs customizations."
  '((emacs "27.0.50")
    (ample-regexps "0.1")
    (f "0.20.0")
    (hydra "0.14.0")
    (multishell "1.1.5")
    (s "1.12.0"))
  :url "https://bitbucket.org/richkinder/emisc")
