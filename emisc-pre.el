 ;;; emisc-pre.el --- Miscellaneous custom functions

;; Copyright (C) 2019 Richard Kinder

;;; Commentary:

;; This is a collection of miscellaneous functions to be loaded before other packages.

;;; Code:


;;; exwm helpers

(require 'cl)
(require 'cl-macs)
(require 'cl-seq)
(require 'frame)
(defvar emisc-exwm-restart-env "RESTARTING_EXWM"
  "Environment variable read by init.el to determine if EXWM is restarting.")

(defun emisc-monitor-names-l-to-r ()
  "Current monitor names, in left-to-right order."
  (cl-loop for mon in (cl-sort (display-monitor-attributes-list)
                               (lambda (mon0 mon1)
                                 (< (cadadr mon0) (cadadr mon1))))
           collect (cdar mon)))

(defun emisc-frames-l-to-r ()
  "Current frames, in left-to-right order."
  (cl-loop for mon in (cl-sort (display-monitor-attributes-list)
                               (lambda (mon0 mon1)
                                 (< (cadadr mon0) (cadadr mon1))))
           collect (cadr (elt mon 4))))

(defun emisc-selected-frame-ind ()
  "Index of current frame, in a l-to-r sense."
  (position (selected-frame) (emisc-frames-l-to-r)))

(defun emisc-exwm-kill-other-emacsen ()
  "Kill other emacs processes if restarting exwm."
  (interactive)
  (let ((emacs-pids
         (loop for pid in (list-system-processes)
               when (string-match-p "emacs"
                                    (cdr (assq 'args
                                               (process-attributes pid))))
               collect pid)))
    (loop for pid in emacs-pids
          when (not (eq pid (emacs-pid)))
          do (signal-process pid 'kill))))

(defcustom emisc-exwm-buffer-translations nil
  "Alist of mappings from `exwm-title' to buffer name, in order of priority."
  :type '(alist :key-type regexp :value-type string)
  :group 'emisc)

(defun emisc-exwm-buffer-translate (str)
  "Translate STR according to `emisc-exwm-buffer-translations'"
  ;; Append a catchall to the end.
  (let ((choices (append emisc-exwm-buffer-translations
                         (list (cons ".*" str))))
        foundp)
    (while (not foundp)
      (setq item (pop choices))
      ;; Add a \' to the the end of regexp to match only once. See
      ;; `replace-regexp-in-string' doc for more info.
      (let* ((regexp (concat (car item) "\\'"))
             (replacement (cdr item)))
        (when (string-match regexp str)
          (setq str (replace-regexp-in-string regexp replacement str))
          (setq foundp t))))) 
  str)

(defun emisc-shell-fix-ps1 ()
  ;; Wait for the prompt to be inserted. I feel like there's a better way to do
  ;; this but not sure how. Without the sleep or with a sleep of zero it loops
  ;; infinitely, so I guess the sleep yields control to an IO process that can
  ;; insert into the buffer.
  (while (eq 1 (point-max))
    (sleep-for 0 1))
  (insert "export PS1='\\u@\\H:\\w$ '")
  (comint-send-input)
  (delete-region 1 (point)))

(provide 'emisc-pre)

;;; emisc-pre.el ends here
